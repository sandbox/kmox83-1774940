<?php
  /**
   * @file
   * Administration page callbacks for the Proliker module.
   */
  /**
   * Form builder. Configure Proliker.
   *
   * @ingroup forms
   * @see system_settings_form().
   */
function proliker_admin_settings() {
  // Get an array of node types with internal names as keys and
  // "friendly names" as values. E.g.,
  // array('page' => ’Basic Page, 'article' => 'Articles')
  $types = array(
    'love' => 'love',
    'use' => 'use',
    'take' => 'take',
    'attend' => 'attend',
    'buy' => 'buy',
    'collect' => 'collect',
    'play' => 'play',
    'wear' => 'wear'
  );

  $form['proliker_app_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Insert the App id'),
    '#default_value' => variable_get('proliker_app_id', ''),
    '#description' => t('The App ID url that will be used for the Proliker widget.'),
  );

  $form['proliker_endpoint_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Insert the endpoint url'),
    '#default_value' => variable_get('proliker_endpoint_url', ''),
    '#description' => t('The endpoint url that will be used for the Proliker widget.'),
  );


  $form['proliker_action_types'] = array(
    '#type' => 'radios',
    '#title' => t('Select which action you want to use'),
    '#options' => $types,
    '#default_value' => variable_get('proliker_action_types', 'love'),
    '#description' => t('The action that will be used for the Proliker widget.'),
  );



  $form['#submit'][] = 'proliker_admin_settings_submit';
  return system_settings_form($form);
}


/**
 * Process proliker settings submission.
 */
function proliker_admin_settings_submit($form, $form_state) {
  variable_set('proliker_action_types', $form_state['values']['proliker_action_types']);
  variable_set('proliker_app_id', $form_state['values']['proliker_app_id']);
  variable_set('proliker_endpoint_url', $form_state['values']['proliker_endpoint_url']);
}